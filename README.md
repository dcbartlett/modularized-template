# Title

[![Discord](https://img.shields.io/discord/123.svg?logo=discord)](https://discord.gg/)
[![Build status](https://gitlab.com/dcbartlett/modularized-template/badges/bundle/TA-Main/pipeline.svg)](https://gitlab.com/dcbartlett/modularized-template/commits/bundle/TA-Main)
[![Coverage Status](https://gitlab.com/dcbartlett/modularized-template/badges/bundle/TA-Main/coverage.svg)](https://gitlab.com/dcbartlett/modularized-template/commits/bundle/TA-Main)
<!--[![Docker Build Status](https://img.shields.io/docker/build/modularized-template/modularized-template.svg)](https://hub.docker.com/r/dcbartlett/modularized-template/tags/)-->

## Contribute

Please contribute! This is an open source project. If you would like to report a bug or suggest a feature, [open an issue](https://github.com/dcbartlett/modularized-template/issues). Or, to open a Pull Request:

1.  Fork it (<http://github.com/dcbartlett/modularized-template/fork>)
2.  Create your feature branch (`git checkout -b my-new-feature`)
3.  Commit your changes (`git commit -am 'Add some feature'`)
4.  Push to the branch (`git push origin my-new-feature`)
5.  Create a new Pull Request

**Before creating your pull request:**

1.  Ensure your code matches our existing style using our provided [EditorConfig](http://editorconfig.org/) options.
2.  Ensure the existing tests pass, or are updated appropriately, with `yarn test`.
3.  For new features, you should add new tests.

### Building and viewing the docs locally

Documentation contributions are always welcome and very appreciated!

modularized-template's documentation site, is automatically generated based on modularized-template's JSDoc comments and the markdown files in the [`tutorials`](https://github.com/dcbartlett/modularized-template/tree/master/tutorials) folder. The [table of contents](#table-of-contents) in this README is also automatically generated.

To build the docs, run the following commands (after you have cloned ClapBoard and installed its dependencies via `npm install`):

```bash
yarn run docs:build
```

After that, you can open `docs/index.html` directly in your web browser.

Once you've made your changes, follow the steps above in the [Contribute](#contribute) section to open a pull request.

### Running tests locally

You no longer need Selenium to run browser integration tests. Chromium is installed with
`yarn install`/`yarn ci`, and that's all required for tests.

Then just run `yarn test`

### Code of Conduct

Note that all contributions and discussions around modularized-template take place under our [Code of Conduct](CODE_OF_CONDUCT.md).
